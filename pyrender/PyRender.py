#!/usr/bin/env python3

from datetime import datetime, timedelta
import random
from selenium import webdriver
from time import sleep
import os

class PyRender:
    '''
    Selenium interface for rendering.
    '''

    class TimeoutError(RuntimeError):
        pass

    class LocalStorage:
        '''
        https://stackoverflow.com/questions/46361494/how-to-get-the-localstorage-with-python-and-selenium-webdriver
        '''

        def __init__(self, driver) :
            self.driver = driver

        def __len__(self):
            return self.driver.execute_script("return window.localStorage.length;")

        def items() :
            return self.driver.execute_script( \
                "var ls = window.localStorage, items = {}; " \
                "for (var i = 0, k; i < ls.length; ++i) " \
                "  items[k = ls.key(i)] = ls.getItem(k); " \
                "return items; ")

        def keys() :
            return self.driver.execute_script( \
                "var ls = window.localStorage, keys = []; " \
                "for (var i = 0; i < ls.length; ++i) " \
                "  keys[i] = ls.key(i); " \
                "return keys; ")

        def get(self, key):
            return self.driver.execute_script("return window.localStorage.getItem(arguments[0]);", key)

        def set(self, key, value):
            self.driver.execute_script("window.localStorage.setItem(arguments[0], arguments[1]);", key, value)

        def has(self, key):
            return key in self.keys()

        def remove(self, key):
            self.driver.execute_script("window.localStorage.removeItem(arguments[0]);", key)

        def clear(self):
            self.driver.execute_script("window.localStorage.clear();")

        def __getitem__(self, key) :
            value = self.get(key)
            if value is None :
              raise KeyError(key)
            return value

        def __setitem__(self, key, value):
            self.set(key, value)

        def __contains__(self, key):
            return key in self.keys()

        def __iter__(self):
            return self.items().__iter__()

        def __repr__(self):
            return self.items().__str__()

    _DRIVER_LOCATION = os.path.expanduser('~/chromedriver')
    _UAS = (
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1", 
        "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0",
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
    )

    def __init__(self):
        options = webdriver.ChromeOptions()

        options.add_argument('headless')
        options.add_argument('user-agent=' + random.choice(PyRender._UAS))
        
        self._driver = webdriver.Chrome(PyRender._DRIVER_LOCATION, chrome_options=options)

        self.localStorage = PyRender.LocalStorage(self._driver)

    def open(self, uri):
        self._driver.get(uri)

        return self

    def wait(self, millis):
        sleep(millis / 1000.0)

        return self

    def capture(self, path, *, minPageHeight=0):
        original_size = self._driver.get_window_size()
        required_width = self._driver.execute_script('return document.body.parentNode.scrollWidth')
        required_height = self._driver.execute_script('return document.body.parentNode.scrollHeight')

        required_height = max(minPageHeight, required_height)
        
        self._driver.set_window_size(required_width, required_height)
 
        self._driver.save_screenshot(path)

        return self

    def runJS(self, js):
        return self._driver.execute_script(js)

    def waitForJS(self, js, timeoutSeconds):
        timeout = timedelta(seconds=timeoutSeconds)
        start = datetime.now()
        result = self.runJS(js)

        while not result and (datetime.now() - start) < timeout:
            result = self.runJS(js)

        if not result:
            raise PyRender.TimeoutError

    def close(self):
        self._driver.close()

    def getById(self, elementId):
        return self._driver.find_element_by_id(elementId)

    def getByName(self, name):
        return self._driver.find_element_by_name(name)

    def navigateTo(self, uri):
        self._driver.get(uri)

    def selectOption(self, element, text):
        for option in element.find_elements_by_tag_name('option'):
            if option.text == text:
                option.click()
                break

    def click(self, element):
        actions = ActionChains(self._driver)
        actions.move_to_element(element)
        actions.click(element)
        actions.perform()
        sleep(0.1)

    def type(self, element, text):
        for ch in text:
            element.send_keys(ch)
            sleep(0.01)

    def close(self):
        self._driver.close()

    def navigateBack(self):
        self._driver.execute_script("window.history.go(-1)")

    def getLinkElements(self):
        links = self._driver.find_elements_by_tag_name('a')

        return [link for link in links if link is not None]

    def getLinkElement(self, href):
        for element in self.getLinkElements():
            if element.get_attribute('href') == href:
                return element

        return None

    def getLinksOnPage(self, contains=None):
        links = self.getLinkElements()

        if contains is not None:
            result = []

            for link in links:
                if link is not None:
                    href = link.get_attribute('href')
                    if href is not None and contains in str(href):
                        result.append(link)

            return result

        return link

if __name__ == '__main__':
    pass
